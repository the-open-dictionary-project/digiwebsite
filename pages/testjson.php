<div id="testJson" class="tab">
	<div>
		<select id="lang">
			<option value="cy">Welsh</option>
			<option value="kw">Cornish</option>
			<option value="ga">Irish</option>
			<option value="br">Breton</option>
			<option value="gd">Scottish Gaelic</option>
		</select>
		<select id="type">
			<option value="verb">Verb</option>
			<option value="adjective">Adjective</option>
			<option value="noun">Noun</option>
			<option value="adposition">Adposition</option>
		</select>
	</div>
	<div>
		<input type="text" id="json" placeholder="Paste JSON here..." onkeypress="return keyPress(event, generateWord)"/>
		<input type="button" value="Submit" onclick="generateWord()">
	</div>
</div>
<div id="results" class="results"></div>		

