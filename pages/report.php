<div class="results">
	<div class="result">
		<h2>Report an Issue</h2>
		<h3>Report Information</h3>
		<select id="lang">
			<option value="br"><?=s('breton')?></option>
			<option value="kw"><?=s('cornish')?></option>
			<option value="ga"><?=s('irish')?></option>
			<option value="gd"><?=s('scottish_gaelic')?></option>
			<option value="cy" selected><?=s('welsh')?></option>
		</select>
		<select id="type">
			<option value="missing-word">Missing word</option>
			<option value="missing-trans">Missing translation</option>
			<option value="wrong-information">Wrong information</option>
			<option value="other">Other</option>
		</select>
		<h3>Word Information</h3>
		<h4>Word</h4>
  		<input type="text" id="word" name="word"><br>
		<h4>Id (if known)</h4>
		<input type="number" id="id" name="id">
		<h3>Additional Information</h3>
		<textarea name="info" rows="25"></textarea>
		<h3>Submit Report</h3>
		<center>
		<input type="button" class="go" value="Submit" onclick="submitReport()">
		</center>
	</div>
</div>

