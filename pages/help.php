<div id="about" class="results">
	<div class="result">
		<h2>Help page</h2>
		<p>
		This page is meant to give a brief overview of some of the things to be aware of while using the dictionary.
		If there is anything missing from this page that you would like to see added please don't hesitate to get in touch with the team!
		</p>
		<h3>Using the dictionary</h3>
		<h4>Word types</h4> 
		<p>
		Word types are used for generating the results. The word type 'all' can be used, but will only work for confirmed words (i.e words that are in the dictionary).
		Searches with the word type 'all' will never generate the result.
		</p>
		<h4>Search types</h4>
		<p>
		<strong>Search by dictionary entry</strong> is used to lookup words by their dictionary entry (i.e the lemma of the word). If a word is not present in the dictionary it will be
		generated if the word type is not set to 'all'.
		</p>
		<p>
		<strong>Extensive search</strong> is used to lookup words by conjugated terms. This implies the word type 'all' and will never generate a result. 
		</p>
		<h3>Searching for translations</h3>
		<p>
			If you know the English translation of a word you can use the search function under the 'Translations' tab.
			This will list all of the words in the different languages that is listed as translations of the English word.
		</p>
		<p>
			<strong>Note</strong> that not all words have translations and some dictionaries have less translations than others.
			If you find any missing translations feel free to report it to the team or submit a merge request on Gitlab!
		</p>
		<p>
			<strong>Verbs</strong> are listed by their infitive version. That will say that all verbs starts with the word 'to' like 'to go' and so on.
		</p>
	</div>
</div>
