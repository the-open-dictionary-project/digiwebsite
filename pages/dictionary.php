<div id="searchSettings" class="tab">
	<div>
		<select id="lang" class="select-css" onchange="changeColor()">
			<option value="br"><?=s('breton')?></option>
			<option value="kw"><?=s('cornish')?></option>
			<option value="de"><?=s('german')?></option>
			<option value="ga"><?=s('irish')?></option>
			<option value="gd"><?=s('scottish_gaelic')?></option>
			<option value="es"><?=s('spanish')?></option>
			<option value="cy" selected><?=s('welsh')?></option>
		</select>
		<select id="type" class="select-css" >
			<option value="verb"><?=s('verb')?></option>
			<option value="adjective"><?=s('adjective')?></option>
			<option value="noun"><?=s('noun')?></option>
			<option value="adposition"><?=s('adposition')?></option>
			<option value="conjunction"><?=s('conjunction')?></option>
			<option value=""><?=s('all')?></option>
		</select>
		<select id="selectSearch" class="select-css">
			<option value="false"><?=s('dictionary_entry_search')?></option>
			<option value="true"><?=s('extensive_search')?></option>
		</select>
	</div>
	<div>
		<input type="text" id="search" placeholder="<?=s('placeholder_type_word')?>" onkeypress="return keyPress(event, wordSearch)"/>
		<input type="button" class="go" value="<?=s('submit')?>" onclick="wordSearch()">
	</div>
</div>
<div id="results" class="tab results"></div>		

<?php
if(!isset($_GET['dict']))
	return;
?>
<script defer>
document.getElementById('lang').value = '<?= $_GET['dict'] ?>';
<?php 
if(isset($_GET['id']))
	echo "queue.push(() => fetchById('{$_GET['dict']}',{$_GET['id']}))\n";
if(isset($_GET['word'])){
	$type = isset($_GET['type']) ? $_GET['type'] : "verb";
	$search = isset($_GET['search']) ? $_GET['search'] : false;

	// Set drop down boxes
	echo "document.getElementById('type').value = '{$type}';\n";
	echo "document.getElementById('selectSearch').value = '{$search}';\n";

	// Fetch word
	echo "queue.push(() => fetchWord(";
	echo "'{$_GET['dict']}', ";
	echo '"', $_GET['word'], "\", ";
	echo (isset($_GET['type']) ? "'{$type}'" : "") . ", ";
	echo $search;
	echo "));\n";
}
?>
</script>
