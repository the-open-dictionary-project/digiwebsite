
<div id="transSettings" class="tab" >
	<input type="text" id="transSearch" placeholder="<?=s('placeholder_type_english_word')?>" onkeypress="return keyPress(event, transSearch)"/>
	<input type="button" value="<?=s('submit')?>" onclick="transSearch()">
</div>
<div id="results" class="tab results"></div>		

<?php
if(!isset($_GET['word']))
	return;
?>
<script>
queue.push(() => fetchTrans(<?php echo "'{$_GET['word']}'" ?>));
</script>
